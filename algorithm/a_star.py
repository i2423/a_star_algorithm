# This file implements "A star" algorithm - implemented by: AndresFWilT
# A* or "A star" is the combination of Uniform-cost and Greedy
# Uniform-cost orders by path cost or backward cost - g(n)
# Greedy orders by goal proximity or forward cost - h(n)
# A* Search orders by the sum: f(n) = g(n) + h(n)

import heapq # priority queue
from collections import defaultdict
from platform import node # dictionary of lists


# class that represents a priority queue
class PriorityQueue:

    def __init__(self):
        self._queue = []
        self._index = 0

    def insert(self, item, priority):
        heapq.heappush(self._queue, (priority, self._index, item))
        self._index += 1

    def remove(self):
        return heapq.heappop(self._queue)[-1]

    def isEmpty(self):
        return len(self._queue) == 0

    def getSize(self):
        return self._index


# class that represents a node
class Node:

    # "key" is the identifier of node
    # "forward_cost" is h(n) (cost of the heuristic)
    # "forward_cost" is used in the calculate of "A* search": f(n) = g(n) + h(n) where
    # h(n) is the forward cost and g(n) is the backward cost
    # remember: "A* search" is the combination of Uniform-cost (UCS) and Greedy
    def __init__(self, key, forward_cost):
        self.key = key
        self.forward_cost = forward_cost

    def getKey(self):
        return self.key

    def getForwardCost(self):
        return self.forward_cost


# class that represents a graph
class Graph:

    def __init__(self):
        self.nodes = {} # dictionary of the nodes
        self.edges = [] # list of 3-tuple (source, destination, weight)
        self.path = [] # path

        # dictionary with the lists of successors of each node, faster for get the successors
        # each item of list is a 2-tuple: (destination, weight)
        self.successors = defaultdict(list)


    # function that adds edges
    def addEdge(self, source, destination, weight):
        edge = (source, destination, weight) # creates tuple (3-tuple)
        if not self.existsEdge(edge): # adds edge if not exists
            self.nodes[source], self.nodes[destination] = source, destination # adds the nodes
            self.edges.append(edge) # adds edge
            self.successors[source.getKey()].append((destination, weight)) # adds successor
        else:
            print('Error: edge (%s -> %s with weight %s) already exists!!' \
                % (edge[0].getKey(), edge[1].getKey(), edge[2]))


    # function that checks if edge exists
    def existsEdge(self, edge):
        for e in self.edges:
            # compares source's key, destionation's key and weight of edge
            if e[0].getKey() == edge[0].getKey() and \
                e[1].getKey() == edge[1].getKey() and e[2] == edge[2]:
                return True
        return False


    # function that returns the path
    def getPath(self):
        return self.path


    # function that run the "A*" algorithm
    def executeAStar(self, initial_node, goal_node):
        if not self.edges:
            print('Error: graph not contains edges!!')
        else:
            # checks if both the nodes exists
            if initial_node in self.nodes and goal_node in self.nodes:
                if initial_node == goal_node: # checks if are the same nodes
                    return 0

                queue = PriorityQueue() # creates a priority queue (min heap)

                # "distance_vector" and "antecessors" are used for reconstruct the path
                distance_vector, antecessors = {}, {}
                for node in self.nodes:
                    distance_vector[node.getKey()] = None # initializes with None
                    antecessors[node.getKey()] = None
                distance_vector[initial_node.getKey()] = 0

                # calculates costs
                g_cost, h_cost = 0, initial_node.getForwardCost()
                f_cost = g_cost + h_cost
                queue.insert((initial_node, g_cost, h_cost), f_cost)
                total_cost = None

                while True:

                    # a item of the queue is a 3-tuple: (current_node, g_cost, h_cost)
                    current_node, g_cost, h_cost = queue.remove()

                    # gets all the successors of "current_node"
                    successors = self.successors[current_node.getKey()]

                    for successor in successors:
                        destination, weight = successor # unpack 2-tuple successor

                        # calculates costs
                        new_g_cost = g_cost + weight
                        h_cost = destination.getForwardCost()
                        f_cost = new_g_cost + h_cost
                        queue.insert((destination, new_g_cost, h_cost), f_cost)

                        # updates "distance_vector"
                        if distance_vector[destination.getKey()]:
                            if distance_vector[destination.getKey()] > new_g_cost:
                                distance_vector[destination.getKey()] = new_g_cost
                                antecessors[destination.getKey()] = current_node.getKey()
                        else:
                            distance_vector[destination.getKey()] = new_g_cost
                            antecessors[destination.getKey()] = current_node.getKey()

                        # verifies that reached the goal
                        if destination.getKey() == goal_node.getKey():
                            # updated "total_cost"
                            if not total_cost:
                                total_cost = f_cost
                            elif f_cost < total_cost:
                                total_cost = f_cost

                    if queue.isEmpty(): # verifies if the queue is empty
                        # reconstruct the path
                        curr_node = goal_node.getKey()
                        while curr_node:
                            self.path.append(curr_node)
                            curr_node = antecessors[curr_node]
                        self.path = self.path[::-1]
                        return total_cost
            else:
                print('Error: the node(s) not exists in the graph!!')

# test with the Graph of the tree from the problem (Conservation of the species from deforestation core in Col)
# creates the nodes
# NodeName = Node(name , cost)
# Comment notation => the father of the node is A : (f*-A)
nodeA = Node('A',80)    ##First level (root)
nodeB = Node('B',70)    ##second level nodes f*-A
nodeC = Node('C',90)    #                   f*-A
nodeD = Node('D',45)    ##third level nodes f*-B
nodeE = Node('E',55)    #                   f*-B
nodeF = Node('F',60)    #                   f*-C
nodeG = Node('G',65)    #                   f*-C
nodeH = Node('H',35)    ##fourth level nodes f*-D
nodeI = Node('I',45)    #                   f*-E
nodeJ = Node('J',50)    #                   f*-F    
nodeK = Node('K',55)    #                   f*-G
nodeL = Node('L',25)    #fifth level nodes  f*-H
nodeM = Node('M',10)    #                   f*-H
nodeN = Node('N',20)    #                   f*-E
nodeO = Node('O',30)    #                   f*-J
nodeP = Node('P',45)    #                   f*-J
nodeQ = Node('Q',30)    #                   f*-K
nodeR = Node('R',10)    #                   f*-K
nodeS = Node('S',25)    #                   f*-K
nodeT = Node('T',15)    #sixth level nodes  f*-L
nodeU = Node('U',35)    #                   f*-M
nodeV = Node('V',45)    #                   F*-N
nodeW = Node('W',45)    #                   F*-O
nodeX = Node('X',35)    #                   f*-P
nodeY = Node('Y',65)    #                   f*-Q
nodeZ = Node('Z',50)    #                   f*-R
nodeAA = Node('AA',40)  #                   f*-S
nodeAB = Node('AB',9)   #leafs              f*-T
nodeAC = Node('AC',5)   #                   f*-T
nodeAD = Node('AD',7)   #                   f*-U
nodeAE = Node('AE',5)   #                   f*-U
nodeAF = Node('AF',10)  #                   f*-V
nodeAG = Node('AG',9)   #                   f*-V
nodeAH = Node('AH',6)   #                   f*-W
nodeAI = Node('AI',5)   #                   f*-W
nodeAJ = Node('AJ',8)   #                   f*-X
nodeAK = Node('AK',7)   #                   f*-X
nodeAL = Node('AL',2)   #                   f*-Y
nodeAM = Node('AM',3)   #                   f*-Y
nodeAN = Node('AN',0)   #                   f*-Z
nodeAO = Node('AO',2)   #                   f*-Z
nodeAP = Node('AP',3)   #                   f*-AA
nodeAQ = Node('AQ',3)   #                   f*-AA

# creates graph
graph = Graph()

# add the edges first node, son, distance
graph.addEdge(nodeA,nodeB, 2)
graph.addEdge(nodeA,nodeC, 3)
graph.addEdge(nodeB,nodeD, 2)
graph.addEdge(nodeB,nodeE, 3)
graph.addEdge(nodeC,nodeF, 3)
graph.addEdge(nodeC,nodeG, 5)
graph.addEdge(nodeD,nodeH, 1)
graph.addEdge(nodeE,nodeI, 1)
graph.addEdge(nodeF,nodeJ, 1)
graph.addEdge(nodeG,nodeK, 1)
graph.addEdge(nodeH,nodeL, 3)
graph.addEdge(nodeH,nodeM, 4)
graph.addEdge(nodeI,nodeN, 1)
graph.addEdge(nodeJ,nodeO, 3)
graph.addEdge(nodeJ,nodeP, 2)
graph.addEdge(nodeK,nodeQ, 3)
graph.addEdge(nodeK,nodeR, 4)
graph.addEdge(nodeK,nodeS, 2)
graph.addEdge(nodeL,nodeT, 1)
graph.addEdge(nodeM,nodeU, 1)
graph.addEdge(nodeN,nodeV, 1)
graph.addEdge(nodeO,nodeW, 1)
graph.addEdge(nodeP,nodeX, 1)
graph.addEdge(nodeQ,nodeY, 1)
graph.addEdge(nodeR,nodeZ, 1)
graph.addEdge(nodeS,nodeAA, 1)
graph.addEdge(nodeT,nodeAB, 1)
graph.addEdge(nodeT,nodeAC, 5)
graph.addEdge(nodeU,nodeAD, 2)
graph.addEdge(nodeU,nodeAE, 4)
graph.addEdge(nodeV,nodeAF, 2)
graph.addEdge(nodeV,nodeAG, 3)
graph.addEdge(nodeW,nodeAH, 2)
graph.addEdge(nodeW,nodeAI, 3)
graph.addEdge(nodeX,nodeAJ, 1)
graph.addEdge(nodeX,nodeAK, 2)
graph.addEdge(nodeY,nodeAL, 4)
graph.addEdge(nodeY,nodeAM, 3)
graph.addEdge(nodeZ,nodeAN, 5)
graph.addEdge(nodeZ,nodeAO, 3)
graph.addEdge(nodeAA,nodeAP, 4)
graph.addEdge(nodeAA,nodeAQ, 4)

total_cost = graph.executeAStar(nodeA, nodeAB) # executes the algorithm
path = graph.getPath() # gets path
if total_cost:
    print('\nTotal cost of graph: %s.\t Path: %s' % (total_cost, ', '.join(path)))
else:
    print('Did not reach the goal!')